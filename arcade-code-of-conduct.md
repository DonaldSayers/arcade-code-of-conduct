# Killer Queen Chattanooga: Code of Conduct

TL;DR: Be an adult. Be nice. We're all friends here.

Version: 3. Last updated: 2023-03-07.

---

Killer Queen Chattanooga does not allow abusive behavior at our tournaments and
League Nights. This includes, but is not limited to;

1. taunting other players or teams (e.g. taunting while playing, taunting as a
   spectator or cheering against a team directly in front of you),
2. singling out other players,
3. talking to the other team during play,
4. excessive celebrations (e.g., loudly celebrating a win over a new or poorly
   matched team),
5. excessive cursing about characters in the abstract (e.g., calling the Queen a
   bitch, slut, cunt, etc), and
6. intimidating players or teams off the cabinet or on (this includes making
   unwanted sexual advances).

If players or teams are behaving in any manners above, a Tournament Organizer,
as defined and chosen by the rules of this tournament, henceforth referred to as
"TO", has the ability to rule on the players or teams in the following way:

After a first offense, the TO has the authority to issue a verbal warning
including an explanation of the rules and what subsequent actions could result
in. If the player commits a second offense, however, whether similar or unique
to the original, it is grounds for the TO to force that player's team to lose
their next series or their current one if they are actively playing. On the
third offense, the TO has the power to remove that player or team from the
tournament entirely, including suggesting removal from the tournament grounds.
Ultimately, the bartender is free to decide whether a player should be removed,
even if a TO does not suggest doing so.

## Anti-Harassment Policy

Harassment and hostility will not be tolerated during or outside of league and
tournament activities. We strive to create an accepting environment and do not
allow harassment of any kind. Harassment includes offensive verbal comments
pertaining to the mission statement.

Harassment also includes:

- Ableism
- Ageism
- Body Shaming
- Bullying
- Disrespectful Behavior
- Homophobia
- Racism
- Sexism
- Transphobia
- Deliberate intimidation
- Inappropriate photography or recording
- Inappropriate electronic communication
- Inappropriate physical contact and unwanted sexual attention
- Racial slurs
- Stalking
- Theft
- Other inappropriate behavior

Harassment of someone not affiliated with an event which occurs during league
and tournament play also falls under these guidelines. Additionally, harassment
transpiring in any event, regardless of whether or not it is held by a league or
tournament which utilizes this code of conduct, may be subject to the same
repercussions discussed below at league officials’ discretion.

Any harassment, expressed threats, or violent actions may result in immediate
ejection from league or tournament and a permanent ban from future events.
Actions will also be reported to the venue and may be subject to discipline per
the venue’s discretion.

Players are encouraged to report this behavior whether or not they were involved
in the exchange. The anonymity of the person(s) reporting issues will be
maintained at all times. Players are also encouraged to point out any time a TO
violates this code of conduct in any way.

We take accusations outside of play very seriously.

## Additional notes

Accidents do happen and some habits are difficult to break, hence enforcement
will occur at the TO and community's discretion. That is, if the hype feels
positive and not as ill will, especially between established players, no action
may be taken unless concerns are raised by a player. However, please keep in
mind that what is a joke to you may not be a joke to another. This includes
certain memes, which may actually be problematic in a racist, sexist, etc way
you may not yet realize.

Killer Queen Chattanooga Players should also be aware that they are representing
our scene whether it is here in Chattanooga or in other Killer Queen
communities. The same abusive behavior outlined above will not be tolerated
while representing us within the Killer Queen community and ecosystem. This
includes out of town tournaments or events as well as online Killer Queen
communities (Facebook, YouTube, Twitch, etc). Repercussions of abusive behavior
may range anywhere from a warning to expulsion from Killer Queen Chattanooga
events and Bitter Alibi and Exile , depending on the severity of the action(s)
at the discretion of the community, Bitter Alibi and Exile.

## COVID 19

Players are asked to not attend events (or will immediately leave the event) if
they have recently been exposed to Covid-19 or experience symptoms of Covid-19
during or within 10 days of the event.

You may be asked if you have gotten a test, and whether you are positive.

Please don't mess around with this. We have people in the community and friends
and family that are at high risk. If you are uncomfortable disclosing this info
please talk to an organizer.

Knowingly exposing people will result in a ban from all events.

## Acknowledgements

Many thanks to Woody Stanfield and the rest of the Chicago scene for writing and
providing the original Code of Conduct, as well as the Charlotte scene for their
amendments.
